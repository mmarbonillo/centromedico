<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/********************************RUTAS CLIENTES************************************/
Route::post('', 'ClienteController@getClients')->name('clientes.getClients');
Route::post('/getClientes', 'ClienteController@getClientsByCif')->name('clientes.getClientsByCif');
Route::get('/clientes', 'ClienteController@index')->name('clientes.index');
Route::get('/clientesNuevo', 'ClienteController@create')->name('clientes.create');
Route::post('/clientesStore', 'ClienteController@store')->name('clientes.store');
Route::post('/clientesUpdate', 'ClienteController@update')->name('clientes.update');
Route::post('/clientesShow', 'ClienteController@show')->name('clientes.show');
Route::post('/restoClientes', 'ClienteController@restoClientes')->name('clientes.restoClientes');

/********************************RUTAS CITAS************************************/
Route::post('/clientesPorFecha', 'CitaController@getClientsByDate')->name('cita.clientsByDate');
Route::post('/clientesPorRangoFecha', 'CitaController@getCitasEnRango')->name('cita.citasEnRango');
Route::post('/citadia', 'CitaController@getCitasDia')->name('cita.getCitasDia');
Route::post('/store', 'CitaController@store')->name('cita.store');
Route::post('/update', 'CitaController@update')->name('cita.update');
Route::get('/editarCita/{id}', 'CitaController@edit')->name('cita.edit');
Route::delete('/eliminarCita', 'CitaController@destroy')->name('cita.destroy');
Route::post('/comprobarRealizada', 'CitaController@comprobarRealizada')->name('cita.comprobarRealizada');


Route::get('', 'FrontendController@index')->name('frontend.index');