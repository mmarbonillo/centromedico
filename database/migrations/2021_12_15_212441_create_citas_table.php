<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('citas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('razon_social', 150);
            $table->char('cif', 9);
            $table->string('direccion', 100);
            $table->string('municipio', 50);
            $table->string('provincia', 50);
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
