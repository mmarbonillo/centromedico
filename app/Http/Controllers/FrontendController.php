<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cita;
use App\Cliente;

class FrontendController extends Controller
{
    /**
     * METODO PARA OBTENER LA WEB
     * 
     */
    public function index(){
        $clientes = DB::select("SELECT id, razon_social FROM clientes ORDER BY cif");
        $data["clientes"] = $clientes;
        $data["citaModificada"] = false;
        return view('frontend.index', $data);
    }


}