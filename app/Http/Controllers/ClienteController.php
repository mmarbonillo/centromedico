<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Cita;
use DB;
use Illuminate\Http\Request;
use Exception;

class ClienteController extends Controller
{

    public function getClients($citas){
        $clientes = DB::table("clientes")->get();
        return response()->json(["clientes" => $clientes]);
    }

    public function getCifs(){
        $cifs = DB::select("SELECT DISTINCT cif FORM clientes ORDER BY cif")->get();
        return response()->json(["cifs" => $cifs]);
    }

    public function getClientsByCif(Request $r){
        $razon_social = $r->razon_social;
        $clientes = DB::select("SELECT id, cif FROM clientes WHERE razon_social LIKE '".$razon_social."' ORDER BY razon_social");
        return response()->json(["clientes" => $clientes]);
    }

    public function restoClientes(Request $r){
        $ultimoId = $r->ultimo;
        $clientes = DB::select('SELECT * FROM clientes WHERE id >'.$ultimoId);
        return response()->json(["clientes" => $clientes]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $clientes = DB::select("SELECT * FROM clientes ORDER BY id");
        $data["clientes"] = $clientes;
        $razonSocial = DB::select("SELECT DISTINCT razon_social FROM clientes ORDER BY razon_social");
        $data["razonSocial"] = $razonSocial;
        $data["count"] = count($razonSocial);
        return view('frontend.clientes', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.anadirCliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $cliente = new Cliente();
        $cliente->razon_social = $r->razonSocial;
        $cliente->cif = $r->cif;
        $cliente->direccion = $r->direccion;
        $cliente->provincia = $r->provincia;
        $cliente->municipio = $r->municipio;
        $cliente->fecha_inicio = $r->fechaInicio;
        $cliente->fecha_fin = $r->fechaFin;
        $cliente->rec_incluidos = $r->reconocimientos;
        $cliente->rec_hechos = 0;
        $cliente->save();
        $data['result'] = true;
        return redirect()->route('clientes.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Request $r){
        $clientes;
        $municipio = $r->municipio == null ? $municipio = '%' : $municipio = $r->municipio;
        if(isset($r->razonSocial))
            $clientes = DB::select("SELECT * FROM clientes WHERE razon_social LIKE '".$r->razonSocial."' AND municipio LIKE '".$municipio."'");
        $data["clientes"] = $clientes;
        $razonSocial = DB::select("SELECT DISTINCT razon_social FROM clientes ORDER BY razon_social");
        $data["razonSocial"] = $razonSocial;
        $data["count"] = count($razonSocial);
        return view('frontend.clientes', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r){
        $cliente = Cliente::find($r->id);
        $cliente->fill($r->all());
        if($cliente->save()){
            return response()->json(['status' => true, 'cliente' => $cliente]);
        }else{
            return response()->json(['status' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //
    }
}
