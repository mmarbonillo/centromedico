<?php

namespace App\Http\Controllers;

use App\Cita;
use App\Cliente;
use Illuminate\Http\Request;
use Exception;
use DB;

class CitaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r){
        $cita = new Cita();
        $cita->fecha = $r->fecha;
        $cita->hora = $r->hora;
        $cita->cliente_id = $r->cliente;
        $cita->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function show(Cita $cita)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $data["cita"] = Cita::find($id);
        return view('frontend.editarCita', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r){
        $citaRealizada = $r->citaRealizada == "on" ? $citaRealizada = 1 : $citaRealizada = 0;
        //echo($r->fecha);
        $cita = Cita::find($r->id);
        $cita->fecha = $r->fecha;
        $cita->hora = $r->horaCita;
        $cita->hecho = $citaRealizada;
        $cita->save();
        $clientes = DB::select("SELECT id, razon_social FROM clientes ORDER BY cif");
        $data["clientes"] = $clientes;
        $data["citaModificada"] = true;
        return view('frontend.index', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cita  $cita
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $r){
        $cita = Cita::find($r->id);
        if($cita->delete()){
            return response()->json(['status' => true]);
        }else{
            return response()->json(['status' => false]);
        }
    }

    /**
     * SACAR CLIENTES QUE TIENE CITA EL DÍA X
     */
    public function getClientsByDate(Request $r) {
        //RECOJO LOS DATOS MANDADOS POR LA PETICIÓN AJAX Y REALIZO LA CONSULTA A LA BD
        $clientes = [];
        $citas;
        if($r->opcion == 1){
            $fecha = $r->date;
            $citas = DB::select("SELECT * FROM citas WHERE fecha LIKE '".$fecha."' ORDER BY hora");
            for($i = 0; $i < count($citas); $i++){
                $clave = $citas[$i]->cliente_id;
                $cliente = Cliente::find($clave);
                $clientes[] = $cliente;
            }
        }else if($r->opcion == 2){
            $inicio = $r->inicio;
            $fin = $r->fin;
            $citas = DB::select("SELECT * FROM citas WHERE fecha BETWEEN '".$inicio."' AND '".$fin."' ORDER BY fecha, hora");
            for($i = 0; $i < count($citas); $i++){
                $clave = $citas[$i]->cliente_id;
                $cliente = Cliente::find($clave);
                $clientes[] = $cliente;
            }
        }
        //select * from citas where fecha between '2021-12-01' and '2021-12-31' order by fecha,hora;
        return response()->json(["citas" => $citas, "clientes" => $clientes]);
    }

    public function getCitasDia(Request $r){
        $fecha = $r->fecha;
        $horas = DB::select('SELECT hora FROM citas WHERE fecha LIKE "'.$fecha.'"');
        return response()->json(["citasNoDisponibles" => $horas]);
    }

    /* COMPROBAR CITA REALIZADA */
    public function comprobarRealizada(Request $r){
        $citaRealizada = DB::select('SELECT hecho FROM citas WHERE id = '.$r->id);
        return response()->json(["citaRealizada" => $citaRealizada]);
    }
}
