<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    /* DESHABILITAR TIMESTAMPS */
    public $timestamps = false;
    protected $fillable = ['razon_social', 'cif', 'direccion', 'municipio', 'provincia', 'fecha_inicio', 'fecha_fin', 'rec_incluidos', 'rec_hechos'];
    public function citas(){
        return $this->hasOne('App\Cita');
    }
}


/*
CREATE TABLE clientes (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    razon_social VARCHAR(150) NOT NULL,
    cif CHAR(9) UNIQUE NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    municipio VARCHAR(50) NOT NULL,
    provincia VARCHAR(50) NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    rec_incluidos VARCHAR(2) NOT NULL,
    rec_hechos VARCHAR(2) NOT NULL DEFAULT 0
);

*/