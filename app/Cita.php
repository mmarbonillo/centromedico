<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $table = 'citas';
    /* DESHABILITAR TIMESTAMPS */
    public $timestamps = false;
    public function clientes(){
        return $this->hasMany('App\Cliente');
    }
}
