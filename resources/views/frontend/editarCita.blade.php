@extends('layouts.frontend')

@section('content')

<!-------------------------------------------------------- EDITAR CITA --------------------------------------------------->

    <div class="col25">
        <!------------------------------------------- FORMULARIO DE EDITAR CITA ------------------------------------------------->
        <form id="addCitaFormForm" class="col100" method="post" enctype="multipart/form-data" action="{{ route('cita.update') }}">
            @csrf
            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="id" value="{{ $cita['id'] }}">

            <label for="fecha" class="col100 mMarginTop">Fecha de la cita</label>
            <input type="date" name="fecha" id="updateFecha" class="col90 mMarginLeft" required value="{{ $cita['fecha'] }}"><br><br>

            <label for="hora" class="col100 mMarginTop">Hora de la cita</label>
            <select name="horaCita" id="horaCita" class="col90 mMarginLeft" required><br><br>
                @for ($i = 8; $i <= 14; $i++)
                    @if ($i < 10)
                        <option class="unaHora" value="0{{ $i }}:00:00">{{ $i }}:00</option>
                        <option class="unaHora" value="0{{ $i }}:15:00">{{ $i }}:15</option>
                        <option class="unaHora" value="0{{ $i }}:30:00">{{ $i }}:30</option>
                        <option class="unaHora" value="0{{ $i }}:45:00">{{ $i }}:45</option>
                    @else
                        <option class="unaHora" value="{{ $i }}:00:00">{{ $i }}:00</option>
                        <option class="unaHora" value="{{ $i }}:15:00">{{ $i }}:15</option>
                        <option class="unaHora" value="{{ $i }}:30:00">{{ $i }}:30</option>
                        <option class="unaHora" value="{{ $i }}:45:00">{{ $i }}:45</option>
                    @endif
                @endfor
            </select>
            <label for="citaRealizada" class="col25 mMarginTop">Cita Realizada</label>
            @if ($cita["hecho"])
                <div class='col5 mMarginRight'><input class='checkRealizada lMarginTop' type='checkbox' name='citaRealizada' checked></div>
            @else
                <div class='col5 mMarginRight'><input class='checkRealizada lMarginTop' type='checkbox' name='citaRealizada'></div>
            @endif
            <div class="col100 centerH">
                <input type="submit" value="Guardar" class="col30 lMarginTop" id="updateCita">
            </div>
        </form>
    </div>

    <script>
        var getCitasDia = "{{ route('cita.getCitasDia') }}";
        var token = "{{ csrf_token() }}";
        var horaCitaActual = "{{ $cita['hora'] }}";
    </script>
    <script src="{{url('js/editarCita.js')}}"></script> 

@endsection