@extends('layouts.frontend')

@section('content')
    
<!------------------------------------------ CONTENIDO ----------------------------------------->
    <div class="col95 bblack">
        <div id="calendario" class="col40 centerV">
            <div id='calendar' class="width100 col100"></div>
        </div>

<!-------------------------------------------- CITAS ------------------------------------------->
        <div id="citas" class="col55 lMarginLeft">
            <div id="listaCitas" class="col95 lMarginLeft">
                <div id="cabecerasCitas" class="hidden col100">
                    <div class='col15 lMarginLeft'>FECHA</div>
                    <div class='col10'>HORA</div>
                    <div class='col10'>RAZÓN SOCIAL</div>
                    <div class='col10 sMarginRight'>CIF</div>
                    <div class='col20'>REALIZADA</div>
                </div>
                <div id="citasDinamico" class="col100"></div>
                
            </div>
        </div>

<!----------------------------------------AÑADIR CITAS ----------------------------------------->

        <div id="anadirCita" class="col55 lMarginLeft hidden">
            <p>Seleccione CIF</p>
            <!-- SELECT CIF DE LA EMPRESA -->
            <select id="rsSelect">
                <option value="0" selected>Seleccione:</option>
                @foreach ($clientes as $cliente)
                    <option value='{{$cliente->razon_social}}'>{{ $cliente->razon_social }}</option>
                @endforeach
            </select>
            <!-- SELECT EMPLEADO -->
            <select name="razonSocialClientes" id="rsClientes">
                <option value="0" selected>Seleccione:</option>
            </select>
            <!-- SELECT HORA DE LA CITA -->
            <select name="horaCita" id="listaHorasCitas" class="hidden">
                @for ($i = 8; $i <= 14; $i++)
                    @if ($i < 10)
                        <option class="unaHora" value="0{{ $i }}:00:00">{{ $i }}:00</option>
                        <option class="unaHora" value="0{{ $i }}:15:00">{{ $i }}:15</option>
                        <option class="unaHora" value="0{{ $i }}:30:00">{{ $i }}:30</option>
                        <option class="unaHora" value="0{{ $i }}:45:00">{{ $i }}:45</option>
                    @else
                        <option class="unaHora" value="{{ $i }}:00:00">{{ $i }}:00</option>
                        <option class="unaHora" value="{{ $i }}:15:00">{{ $i }}:15</option>
                        <option class="unaHora" value="{{ $i }}:30:00">{{ $i }}:30</option>
                        <option class="unaHora" value="{{ $i }}:45:00">{{ $i }}:45</option>
                    @endif
                @endfor
            </select>
            
            <div id="botonGuardar" class="col15 clear right">
                <button id="btnVolverCitas" class="btn-success rounded right mMarginLeft">Volver</button>
                <button id="btnGuardarCita" class="btn-success rounded right hidden">Guardar</button>
            </div>
        </div>
        <div id="botonAnadir" class="col40 clear sMarginTop">
            <button id="btnVerClientes" class="btn-success rounded col20" onclick="window.location.href = '{{ route('clientes.index') }}'">Ver Clientes</button>
            <button id="btnAnadir" class="btn-success rounded col20 right">Añadir</button>
        </div>
    </div>

<!------------------------------------ FUNCIONES Y LISTENERS ---------------------------------->
    
    <script>
        /* TOKEN DE SEGURIDAD PARA AJAX */
        var token = "{{ csrf_token() }}";
        /* RUTAS LARAVEL */
        var getCitasDia = "{{ route('cita.getCitasDia') }}";
        var createCitaUrl = "{{ route('cita.store') }}";
        var clientesByDateUrl = "{{ route('cita.clientsByDate') }}";
        var citaUpdateUrl = "{{ route('cita.update') }}";
        var comprobarCitaRealizadaUrl = "{{ route('cita.comprobarRealizada') }}";
        var modificarCita = "{{ route('cita.edit' , 'id') }}";
        var eliminarCitaUrl  = "{{ route('cita.destroy') }}";
        
        /* EVENTO CHANGE PARA RECONOCER CUANDO SE SELECCIONA UN CIF */
        $('#rsSelect').on('change', function() {
            var razon_social = this.value;
            var route = "{{ route('clientes.getClientsByCif') }}";
            $.ajax({
                url: route,
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "razon_social": razon_social,
                },
                success:function(result){
                    rellenarSelectClientes(result["clientes"]);
                }
            });
        });
        
    </script>
    <script src="{{url('js/index.js')}}"></script> 
    
    
    
@endsection

