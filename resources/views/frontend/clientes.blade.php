@extends('layouts.frontend')

@section('headExtension')
<script src="{{url('js/table2excel/src/jquery.table2excel.js')}}"></script>
<script src="{{url('js/clientes.js')}}"></script>
@endsection

@section('content')

    <div id="contenido" class="col95 bblack">

<!------------------------------------------------------ LISTA DE CLIENTES --------------------------------------------------->
        <div id="listaClientes" class="col100" style="height: 95%; overflow-x: scroll;">
            <!-------------------------- CABECERA ------------------------------->
            <div id="titulos" class="col100">
                <div class="col10"><h5>RAZÓN SOCIAL</h5></div>
                <div class="col10 sMarginLeft"><h5>CIF</h5></div>
                <div class="col15 sMarginLeft"><h5>DIRECCIÓN</h5></div>
                <div class="col10 sMarginLeft"><h5>MUNICIPIO Y PROVINCIA</h5></div>
                <div class="col15 sMarginLeft"><h5>VALIDEZ DEL CONTRATO</h5></div>
                <div class="col10 sMarginLeft"><h5>RECONOCIMIENTOS INCLUIDOS</h5></div>
                <div class="col10 sMarginLeft"><h5>RECONOCIMIENTOS REALIZADOS</h5></div>
                <div class="col10 sMarginLeft"><h5>EDITAR CLIENTE</h5></div>
                <div class="col10 sMarginTop right"><button class="right mMarginRight" onclick="window.location.href='{{ route('clientes.create') }}'">AÑADIR</button></div>
            </div>
            <!-------------------------- CONTENIDO ------------------------------>
            <table id="contenidoClientes" class="col100 contenidoClientes">
                @for ($i = 0; $i < count($clientes); $i++)
                    <!-- ALTERNAR COLORES -->
                    <!-- NO QUITAR CLASE noEx1. ES PARA LA PAGINACIÓN -->
                    @if ($i % 2 == 0)
                        <tr id="{{ $clientes[$i]->id }}" class="unCliente col100 backBlue centerV noExl">
                    @else
                        <tr id="{{ $clientes[$i]->id }}" class="unCliente col100 backOrange centerV noExl">
                    @endif
                        <!-------------------------- LISTA DE CLIENTES ------------------------------->
                        <!--------- Una <p> para listar y un <input> oculto para editar -------------->
                        <td class="col5 sMarginLeft datoCliente{{$clientes[$i]->id}}"><p class="centerV razonSocial">{{ $clientes[$i]->razon_social }}</p></td>
                        <td class="col5 sMarginLeft hidden inputCliente{{$clientes[$i]->id}}"><input class="col100 datoEditar{{$clientes[$i]->id}}" type="text" name="razonSocial" value="{{ $clientes[$i]->razon_social }}"></td>

                        <td class="col10 xxlMarginLeft datoCliente{{$clientes[$i]->id}}"><p class="centerV cif">{{ $clientes[$i]->cif }}</p></td>
                        <td class="col10 xxlMarginLeft hidden inputCliente{{$clientes[$i]->id}}"><input class="col100 datoEditar{{$clientes[$i]->id}}" type="text" name="cif" value="{{ $clientes[$i]->cif }}"></td>

                        <td class="col15 xlMarginLeft datoCliente{{$clientes[$i]->id}}"><p class="centerV direccion" >{{ $clientes[$i]->direccion }}</p></td>
                        <td class="col15 xlMarginLeft hidden inputCliente{{$clientes[$i]->id}}"><input class="col90 datoEditar{{$clientes[$i]->id}}" type="text" name="direccion" value="{{ $clientes[$i]->direccion }}"></td>

                        <td class="col10 sMarginLeft datoCliente{{$clientes[$i]->id}}"><p class="centerV municipioProvincia">{{ $clientes[$i]->municipio }}, {{ $clientes[$i]->provincia }}</p></td>
                        <td class="col10 sMarginLeft hidden inputCliente{{$clientes[$i]->id}}"><input class="col80 datoEditar{{$clientes[$i]->id}}" type="text" name="municipioProvincia" value="{{ $clientes[$i]->municipio }}, {{ $clientes[$i]->provincia }}"></td>

                        <td class="col15 lMarginLeft datoCliente{{$clientes[$i]->id}}"><p class="centerV fechaContrato">{{ $clientes[$i]->fecha_inicio }} - {{ $clientes[$i]->fecha_fin }}</p></td>
                        <td class="col15 xlMarginRight centerV hidden inputCliente{{$clientes[$i]->id}}">
                            <input type="date" name="fechaInicio" class="col50 datoEditar{{$clientes[$i]->id}}" value="{{ $clientes[$i]->fecha_inicio }}">
                            <input type="date" name="fechaFin" class="col50 datoEditar{{$clientes[$i]->id}}" value="{{ $clientes[$i]->fecha_fin }}">
                        </td>

                        <td class="col10 sMarginLeft datoCliente{{$clientes[$i]->id}}"><p class="centerV recIncluidos">{{ $clientes[$i]->rec_incluidos }}</p></td>
                        <td class="col10 sMarginLeft hidden inputCliente{{$clientes[$i]->id}}"><input class="col60 datoEditar{{$clientes[$i]->id}}" type="text" name="recIncluidos" value="{{ $clientes[$i]->rec_incluidos }}"></td>

                        <td class="col10 sMarginLeft datoCliente{{$clientes[$i]->id}}"><p class="centerV recHechos">{{ $clientes[$i]->rec_hechos }}</p></td>
                        <td class="col10 sMarginLeft hidden inputCliente{{$clientes[$i]->id}}"><input class="col60 datoEditar{{$clientes[$i]->id}}" type="text" name="recHechos" value="{{ $clientes[$i]->rec_hechos }}"></td>
                        <!----------- BOTONES DE EDITAR Y GUARDAR CLIENTE ---------->
                        <td class="col10 sMarginLeft datoCliente{{$clientes[$i]->id}}"><button onclick="editarCliente({{ $clientes[$i]->id }})">EDITAR</button></td>
                        <td class="col10 sMarginLeft hidden inputCliente{{$clientes[$i]->id}}"><button onclick="modificarCliente({{ $clientes[$i]->id }})">GUARDAR</button></td>
                    </tr>
                @endfor
            </table>
        </div>
        <input id="pagActual" type="hidden" name="paginaActual" value="1">

        <!----------------------BARRA DE BÚSQUEDA DE CLIENTES ------------------------>
        <form id="addClienteForm" class="col50" method="post" enctype="multipart/form-data" action="{{ route('clientes.show') }}">
            @csrf
            <input type="hidden" name="_method" value="POST">
            <div id="barraBusqueda" class="col100 clear sMarginTop">
                <div class="col30">
                    <!-- RAZÓN SOCIAL -->
                    <label for="razonSocial" class="col40">Razón social: </label>
                    <select name="razonSocial" id="buscarRazonSocial" class="col55 datoBuscar">
                        <option class="unaRazon" value="%" selected>Seleccione</option>
                        @for ($i = 0; $i < $count; $i++)
                            <option class="unaRazon" value="{{ $razonSocial[$i]->razon_social }}">{{ $razonSocial[$i]->razon_social }}</option>
                        @endfor
                    </select>
                </div>
                <!-- MUNICIPIO -->
                <div class="col40 sMarginLeft">
                    <label for="municipio" class="col20">Municipio: </label>
                    <input class="col50 datoBuscar" type="text" name="municipio" default="%">
                </div>
        
                <div class="col10">
                    <input type="submit" value="Buscar" id="buscarCliente">
                </div>
            </div>
        </form>
        <!-- VER TODOS -->
        <form id="addClienteForm" class="col15 sMarginTop" method="get" enctype="multipart/form-data" action="{{ route('clientes.index') }}">
            <input type="submit" value="Ver Todos" id="volverIndex">
        </form>
        <form id="volverCitas" class="col10 sMarginTop right" method="get" enctype="multipart/form-data" action="{{ route('frontend.index') }}">
            <input  type="submit" value="Ver Citas" id="volverCitas">
        </form>
    </div>
    
    <script>
        /* RUTAS PARA EL .js */
       var token = "{{ csrf_token() }}";
       var clientesShowUrl = "{{ route('clientes.show') }}";
       var clientesUpdateUrl = "{{ route('clientes.update') }}";
       var restoClientesUrl = "{{ route('clientes.restoClientes') }}";
       var ultimoCliente = "{{ count($clientes) }}";

/********************************************* PAGINACIÓN **********************************************/
        $("#pagina2").click(function(){
            $("table").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                name: "Worksheet Name",
                filename: "clientes", //do not include extension
                fileext: ".xls", // file extension
                reserveColors:true
            }); 
        });

        $(function(){
            var $table = $('table');  
            var currentPage = 0;// El valor predeterminado de la página actual es 0  
            var pageSize = 16;// Número que se muestra en cada página  
            $table.bind('paging',function(){
                $table.find('tbody tr').hide().slice(currentPage*pageSize,(currentPage+1)*pageSize).show();
            });
            var sumRows = $table.find('tbody tr').length;
            var sumPages = Math.ceil(sumRows/pageSize);//paginas totales

            var $pager = $('<div class="page right mMarginRight"></div>');  // Crea un nuevo div, coloca una etiqueta, muestra el número de página inferior
            for(var pageIndex = 0 ; pageIndex < sumPages ; pageIndex++){
                if(pageIndex < 4){ /* MUESTRO DE LA PÁGINA 1 A LA 4 */
                    $('<a class="enlacePagina" href="#" id="pageStyle'+ (pageIndex + 1 ) +'"><span onclick="pasarPaginas(this, '+sumPages+')" id="'+ (pageIndex + 1) +'">'+(pageIndex+1)+', </span></a>').bind("click",{"newPage":pageIndex},function(event){
                        currentPage = event.data["newPage"];
                        $table.trigger("paging");
                        // Activar la función de paginación
                    }).appendTo($pager);
                    $pager.append(" ");
                    if(pageIndex == 0){ // PONGO LOS PUNTOS SUSPENSIVOS DE DELANTE DE LA PÁGINA 1
                        $pager.append('<a class="sMarginRight puntosSuspensivos hidden" href="#" id="puntosSusp1"><span>...</span></a>');
                    }
                }
                if(pageIndex >= 4 && pageIndex < sumPages - 1){
                    $('<a class="hidden enlacePagina" href="#" id="pageStyle'+ (pageIndex + 1 ) +'"><span onclick="pasarPaginas(this, '+sumPages+')" id="'+ (pageIndex + 1 ) +'">'+(pageIndex+1)+', </span></a>').bind("click",{"newPage":pageIndex},function(event){
                        currentPage = event.data["newPage"];
                        $table.trigger("paging");
                        // Activar la función de paginación
                    }).appendTo($pager);
                    
                }
                if(pageIndex == sumPages - 1){
                    $pager.append('<a class="sMarginRight puntosSuspensivos" href="#" id="puntosSuspUlt"><span>...</span></a>'); // PONGO LOS PUNTOS SUSPENSIVOS DELANTE DE LA ÚLTIMA PÁGINA
                    $('<a class="enlacePagina" href="#" id="pageStyle'+ (pageIndex + 1 ) +'"><span onclick="pasarPaginas(this, '+sumPages+')" id="'+ (pageIndex + 1 ) +'">'+(pageIndex+1)+'</span></a>, ').bind("click",{"newPage":pageIndex},function(event){
                        currentPage = event.data["newPage"];
                        $table.trigger("paging");
                        // Activar la función de paginación
                    }).appendTo($pager);
                }

            }
            $pager.insertAfter($table);  
            $table.trigger("paging");  

            // El efecto predeterminado de una etiqueta en la primera página  
            var $pagess = $('#pageStyle');  
            //$pagess[0].style.backgroundColor="#006B00";  
            //$pagess[0].style.color="#ffffff";  
        });
    </script>
    

@endsection