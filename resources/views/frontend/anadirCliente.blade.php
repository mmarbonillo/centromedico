@extends('layouts.frontend')

@section('content')
<!--------------------------------------------------------- FORMULARIO DE AÑADIR CLIENTE -------------------------------------------------->
    <div class="col25">
        <form id="addClienteForm" class="col100" method="post" enctype="multipart/form-data" action="{{ route('clientes.store') }}">
            @csrf
            <input type="hidden" name="_method" value="POST">

            <label for="razonSocial" class="col100 mMarginTop">Razón Social</label>
            <input type="text" name="razonSocial" id="newClienteRazonsocial" class="col90 mMarginLeft" required><br><br>

            <label for="cif" class="col100 mMarginTop">CIF</label>
            <input type="text" name="cif" id="newClienteCif" class="col90 mMarginLeft" required><br><br>

            <label for="direccion" class="col100 mMarginTop">Dirección</label>
            <input type="input" name="direccion" id="newClienteDireccion" class="col90 mMarginLeft" required><br><br>

            <label for="municipio" class="col100 mMarginTop">Municipio</label>
            <input type="input" name="municipio" id="newClienteMunicipio" class="col90 mMarginLeft" required><br><br>

            <label for="provincia" class="col100 mMarginTop">Provincia</label>
            <input type="input" name="provincia" id="newClienteProvincia" class="col90 mMarginLeft" required><br><br>

            <label for="validezContrato" class="col100 mMarginTop">Validez Contrato</label>
            <input type="date" name="fechaInicio" id="newClienteFechaInicio" class="col40 mMarginLeft" required><br><br>
            <p class="col10 mMarginLeft mMarginRight xsMarginTop centerH">hasta</p>
            <input type="date" name="fechaFin" id="newClienteFechaFin" class="col40" required><br><br>

            <label for="reconocimientos" class="col100 mMarginTop">Reconocimientos Médicos</label>
            <input type="number" name="reconocimientos" id="newClienteReconocimientos" class="col20 mMarginLeft" required><br><br>

            <div class="col100 centerH">
                <input type="submit" value="Guardar" class="col30 lMarginTop" id="addCliente">
            </div>
        </form>
    </div>

@endsection

<!--
CREATE TABLE clientes (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    razon_social VARCHAR(150) NOT NULL,
    cif CHAR(9) UNIQUE NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    municipio VARCHAR(50) NOT NULL,
    provincia VARCHAR(50) NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    rec_incluidos VARCHAR(2) NOT NULL,
    rec_hechos VARCHAR(2) NOT NULL DEFAULT 0
);
-->