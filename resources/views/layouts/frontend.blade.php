<html>
    <head>
        <link rel="stylesheet" href="{{url('css/frontend.css')}}" />
        <link rel="stylesheet" href="{{url('css/global.css')}}" />
        <link rel="stylesheet" href="{{url('css/app.css')}}" />
        <link rel="stylesheet" href="{{url('calendar/main.css')}}" />
        <script src="{{url('calendar/main.js')}}"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="Assets/js/jpaginate.js"></script>
		<link href="https://fonts.googleapis.com/css?family=arial:400, 500, 700&display=swap" rel="stylesheet">
        @yield('headExtension')
        <!-- Por defecto title Celia Tour -->
        <title>
            @yield('title', 'Centro Médico') 
        </title>
    </head>

    <body>       
        <!-- CONTENIDO PRINCIPAL -->
        <main class="col100 centerVH">
            @yield('content')
        </main>
        <!-- VENTANA MODAL -->
        <div id="modalWindow" class="col100">
            @yield('modal')
        </div>
    </body>
</html>