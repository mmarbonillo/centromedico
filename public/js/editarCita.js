function eliminarHoras(){
    /**
     * Si es la segunda vez que pincho en el calendario no aparecerán las horas
     * que se ocultaron la primera vez. Hay que volver a mostrarlas todas
     */
    $(".unaHora").show();
    $.ajax({
        url: getCitasDia,
        type: 'POST',
        data: {
            "_token": token,
            "fecha": $("#updateFecha").val(),
        },
        success:function(result){
            var horas = result['citasNoDisponibles'];
            //console.log(horas);
            if(horas != null){
                //console.log(horas);
                /* ELIMINO LAS HORAS OCUPADAS DEL DÍA EXCEPTO LA DE LA HORA A MODIFICAR LA CUAL SELECCIONO */
                horas.forEach(hora => {
                    if(hora.hora == horaCitaActual){
                        $("#horaCita option[value='" + hora.hora + "']").attr('selected', 'selected');
                    }else{
                        $("#horaCita option[value='" + hora.hora + "']").hide();
                    }
                    
                });
            }
        }
    });
}

/* ELIMINAR HORAS YA COGIDAS EN CITAS PARA EL DÍA DE LA CITA A MODIFICAR */
window.onload = eliminarHoras();

$("#horaCita").on({
    change: function(){
        eliminarHoras();
    }
});