
/************************** FUNCIONES Y VARIABLES *********************************/


var fechaCalendario = "";
var fechaInicioSeleccion = "";
var fechaFinSeleccion = "";

function actualizarCitas(id){
    $("#" + id + ".unaCita").remove();
}

function eliminarCita(id){
    $.ajax({
        url: eliminarCitaUrl,
        type: 'DELETE',
        data: {
            "_token": token,
            "id": id,
        },
        success:function(result){
            if(result["status"]){
                actualizarCitas(id);
                alert("Cita eliminada correctamente");

            }
        }
    });
}

function comprobarCita(e){
    var id = e.id;
    var value = e.value;
    /* COMPROBAR QUE LA CITA NO SE HAYA SEALIZADO */
    $.ajax({
        url: comprobarCitaRealizadaUrl,
        type: 'POST',
        data: {
            "_token": token,
            "id": id,
        },
        success:function(result){
            var hecho = result["citaRealizada"][0]["hecho"];
            if(hecho){
                alert("No puede modificar esta cita porque ya se ha realizado");
            }
            else{
                if(value == "Editar"){
                    var ruta = modificarCita.replace('id', id);
                    window.location.href = ruta;
                }else{
                    var opcion = confirm("¿Está seguro de que desea eliminar la cita?");
                    opcion ? eliminarCita(id) : alert("Cancelar");
                }
                
            }
        }
    });
}

/* MOSTRAR LISTA DE CITAS */
function mostrarCitas(data) {
    var citas = data["citas"];
    var clientes = data["clientes"];
    console.log(citas);
    $("#cabecerasCitas").css("display", "block");
    $("#citasDinamico").empty();
    for(i = 0; i < citas.length; i++){
        var check = "";
        if(citas[i]["hecho"])
            check = "<div class='col5 mMarginRight sMarginTop'><input class='checkRealizada' type='checkbox' name='citaRealizada' id='" + citas[i]["id"] + "' checked disabled></div>";
        else
            check = "<div class='col5 mMarginRight sMarginTop'><input class='checkRealizada' type='checkbox' name='citaRealizada' id='" + citas[i]["id"] + "' disabled></div>";
        var insertar = "<div id='" + citas[i]["id"] + "' class='col100 lMarginLeft mMarginTop unaCita'>" + 
            "<div class='col15 venterH'>" + citas[i]["fecha"] + "</div>" + 
            "<div class='col10 venterH'>" + citas[i]["hora"] + "</div>" + 
            "<div class='col10 venterH'>" + clientes[i]["razon_social"] + "</div>" +
            "<div class='col15 venterH'>" + clientes[i]["cif"] + "</div>" +
            check +
            "<div id='botonesCitas' class='col30'>" + 
                "<div class='editarCita col45 '>" + 
                    "<button id='" + citas[i]["id"] + "' onclick='comprobarCita(this)' type='button' value='Editar' class='modify col80 btnModificarCita'>Editar</button>" + 
                "</div>" + 
                "<div class='eliminarCita col45 '>" + 
                    "<button id='" + citas[i]["id"] + "' onclick='comprobarCita(this)' type='button' value='Eliminar' class='delete col80 btnEliminarCita'>Eliminar</button>" + 
                "</div>" + 
            "</div>" + 
            "</div>";
        $("#citasDinamico").append(insertar);
    }
}

/* RELLENAR SELECT DE CLIENTES PARA DARLE CITA */
function rellenarSelectClientes(clientes){
    var option;
    //console.log(clientes);
    clientes.forEach(cliente => {
        option = "<option value='" + cliente['id'] + "'>" + cliente['cif'] + "</option>"
        $("#rsClientes").append(option);
    });
}

/* ELIMINAR ELEMENTOS OPTION DE LAS HORAS NO DISPONIBLES */
function eliminarHoras(){
    /**
     * Si es la segunda vez que pincho en el calendario no aparecerán las horas
     * que se ocultaron la primera vez. Hay que volver a mostrarlas todas
     */
    $(".unaHora").show();
    $.ajax({
        url: getCitasDia,
        type: 'POST',
        data: {
            "_token": token,
            "fecha": fechaCalendario,
        },
        success:function(result){
            var horas = result['citasNoDisponibles'];
            //console.log(horas);
            if(horas != null){
                //console.log(horas);
                horas.forEach(hora => {
                    $("#listaHorasCitas option[value='" + hora.hora + "']").hide();
                });
            }
        }
    });
}

/* GUARDAR CITA */
function guardarCita(){
    /* RECOGER DATOS */
    var cliente = $("#rsClientes option:selected").val();
    var hora = $("#listaHorasCitas option:selected").val();
    //alert("Cliente: " + cliente + "; hora: " + hora);
    $.ajax({
        url: createCitaUrl,
        type: 'POST',
        data: {
            "_token": token,
            "cliente": cliente,
            "fecha": fechaCalendario,
            "hora": hora,
        },
        success:function(result){
            $("#listaHorasCitas, #btnGuardarCita").hide();
            alert("Cita guardada con éxito");
        }
    });
}

/* FECHA ACTUAL PARA INICIALIZAR EL CALENDARIO */
fecha = new Date();
/* LE SUMAMOS UNO AL MES PORQUE TIENE INDEXACIÓN DE ARRAY (0-11) */
mesActual = fecha.getMonth() + 1;
fechaActual = fecha.getFullYear() + "-" + mesActual + "-" + fecha.getDate();

document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
    headerToolbar: {
        /* posiciones del menú */
        left: 'prev,next',
        center: 'title',
        right: 'today'
    },
    initialDate: fechaActual,
    //navLinks: true, // can click day/week names to navigate views
    //businessHours: true, // display business hours
    editable: true,
    selectable: true,
    events: [
        /*{
        title: 'Business Lunch',
        start: '2020-09-03T13:00:00',
        constraint: 'businessHours'
        },
        {
        title: 'Meeting',
        start: '2020-09-13T11:00:00',
        constraint: 'availableForMeeting', // defined below
        color: '#257e4a'
        },
        {
        title: 'Conference',
        start: '2020-09-18',
        end: '2020-09-20'
        },
        {
        title: 'Party',
        start: '2020-09-29T20:00:00'
        },*/

        // areas where "Meeting" must be dropped
        {
        groupId: 'availableForMeeting',
        start: '2020-09-11T10:00:00',
        end: '2020-09-11T16:00:00',
        display: 'background'
        },
        {
        groupId: 'availableForMeeting',
        start: '2020-09-13T10:00:00',
        end: '2020-09-13T16:00:00',
        display: 'background'
        }/*,

        // red areas where no events can be dropped
        {
        start: '2020-09-24',
        end: '2020-09-30',
        overlap: false,
        display: 'background',
        color: '#ff9f89'
        },
        {
        start: '2020-09-06',
        end: '2020-09-08',
        overlap: false,
        display: 'background',
        color: '#ff9f89'
        }*/
    ]
    });
    /* Añadir evento al click de calendario */
    calendar.on('dateClick', function(info) {
        /* PONGO LA FECHA EN UNA VARIABLE GLOBAL */
        fechaCalendario = info.dateStr;
        /* COMPROBAR LA PANTALLA ACTUAL PARA NO CONSUMIR RED */
        var pantalla = $("#citas").css("display");
        if (pantalla != "none"){
            $.ajax({
                url: clientesByDateUrl,
                type: 'POST',
                data: {
                    "_token": token,
                    "date": fechaCalendario,
                    "opcion": '1',
                },
                success:function(result){
                    console.log(result);
                    mostrarCitas(result);
                }
            });
        }
        /* CONTROLAR SI SE PINCHA EL CALENDARIO CUANDO YA SE HA SELECCIONADO EMPLEADO */
        if($("#listaHorasCitas").css("display") != "none"){
            eliminarHoras();
        }
    });
    calendar.on('select', function(info){
        fechaInicioSeleccion = info.startStr;
        fechaFinSeleccion = info.endStr;
        var pantalla = $("#citas").css("display");
        if (pantalla != "none"){
            $.ajax({
                url: clientesByDateUrl,
                type: 'POST',
                data: {
                    "_token": token,
                    "inicio": fechaInicioSeleccion,
                    "fin": fechaFinSeleccion,
                    "opcion": '2',
                },
                success:function(result){
                    console.log(result);
                    mostrarCitas(result);
                }
            });
        }
        if($("#listaHorasCitas").css("display") != "none"){
            eliminarHoras();
        }
    });
    calendar.render();
});

/* BOTÓN AÑADIR QUE CAMBIA DE PANTALLA */
$("#btnAnadir").click(function(){
    $("#anadirCita").show();
    $("#citas").hide();
    $("#btnAnadir").prop('disabled', true);
});

/* MOSTRAR INPUT DE LA HORA */
$("#rsClientes").on({
    change: function(){
        eliminarHoras();
        $('#listaHorasCitas, #btnGuardarCita').show();
    }
});

/* CONTROLAR QUE HAN SELECCIONADO UN DÍA DEL CALENDARIO */
$('#listaHorasCitas').on({
    click: function(){
        if(fechaCalendario == ""){
            alert("Seleccione una fecha en el calendario");
        }
    }
});

/* GUARDAR CITA */
$("#btnGuardarCita").on({
    click: function(){
        guardarCita();
    }
});

/* BOTÓN VOLVER */
$("#btnVolverCitas").on({
    click: function(){
        $("#anadirCita").hide();
        $("#citas").show();
        $("#btnAnadir").prop('disabled', false);
    }
});

/* BOTÓN MODIFICAR CITA */
$("#btnModificarCita").on({
    click: function(){
        $.ajax({
            url: clientesByDateUrl,
            type: 'POST',
            data: {
                "_token": token,
                "inicio": fechaInicioSeleccion,
                "fin": fechaFinSeleccion,
                "opcion": '2',
            },
            success:function(result){
                console.log(result);
                mostrarCitas(result);
            }
        });
    }
});

