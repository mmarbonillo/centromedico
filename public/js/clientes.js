
var pagActual = 0;

function buscarClientes(){
    /* COMPRUEBO SI EL DATO SE HA RELLENADO Y SI NO LO HA HECHO, COMO CASI TODOS SE BUSCAN COMO STRINGS, LES PONGO % */
    var razonSocial = $('.datoBuscar[name="razonSocial"] option:selected').val();
    var municipio = $('.datoBuscar[name="municipio"]').val() != "" ? municipioProvincia = $('.datoBuscar[name="municipioProvincia"]').val() : "%";
    $.ajax({
        url: clientesShowUrl,
        type: 'POST',
        data: {
            "_token": token,
            "razonSocial": razonSocial,
            "municipio": municipio,
        },
        success:function(result){
            alert("todo ok");
        }
    });
}

/* OCULTAR DATOS Y MOSTRAR INPUTS PARA MODIFICAR */
function editarCliente(id){
    $(".datoCliente" + id).hide();
    $(".inputCliente" + id).show();
} 

function actualizarFila(cliente){
    var id = cliente["id"];
    /* MODIFICO LOS VALORES DE LOS INPUTS POR LOS NUEVOS */
    $('.datoEditar'+ id +'[name="razonSocial"]').val(cliente["razon_social"]);
    $(".datoEditar"+ id +"[name='cif']").val(cliente["cif"]);
    $(".datoEditar"+ id +"[name='direccion']").val(cliente["direccion"]);
    $(".datoEditar"+ id +"[name='municipioProvincia']").val(cliente["municipio"] + ", " + cliente["provincia"]);
    $(".datoEditar"+ id +"[name='fechaInicio']").val(cliente["fecha_inicio"]);
    $(".datoEditar"+ id +"[name='fechafin']").val(cliente["fecha_fin"]);
    $(".datoEditar"+ id +"[name='recIncluidos']").val(cliente["rec_incluidos"]);
    $(".datoEditar"+ id +"[name='recHechos']").val(cliente["rec_hechos"]);
    /* MODIFICO LOS TEXTOS DE LA TABLA */
    $('.datoCliente'+ id +' > p.razonSocial').text(cliente["razon_social"]);
    $(".datoCliente"+ id +" > p.cif").text(cliente["cif"]);
    $(".datoCliente"+ id +" > p.direccion").text(cliente["direccion"]);
    $(".datoCliente"+ id +" > p.municipioProvincia").text(cliente["municipio"] + ", " + cliente["provincia"]);
    $(".datoCliente"+ id +" > p.fechaContrato").text(cliente["fecha_inicio"] + " - " + cliente["fecha_fin"]);
    $(".datoCliente"+ id +" > p.recIncluidos").text(cliente["rec_incluidos"]);
    $(".datoCliente"+ id +" > p.recHechos").text(cliente["rec_hechos"]);
    $(".datoCliente" + id).show();
    $(".inputCliente" + id).hide();
}

function modificarCliente(idCliente){
    var id = idCliente;
    var razonSocial = $('.datoEditar'+ id +'[name="razonSocial"]').val();
    var cif = $(".datoEditar"+ id +"[name='cif']").val();
    var direccion = $(".datoEditar"+ id +"[name='direccion']").val();
    var municipioProvincia = $(".datoEditar"+ id +"[name='municipioProvincia']").val();
    /* BUSCO LA POSICIÓN DE LA COMA PARA SEPARAR EL MUNICIPIO DE LA PROVINCIA */
    var coma = municipioProvincia.search(",");
    var municipio = municipioProvincia.substring(0, coma);
    /* LE SUMO 2 A LA POSICIÓN PARA QUE SE POSICIONE EN LA PRIMERA LETRA DE LA PROVINCIA */
    var provincia = municipioProvincia.substring(coma+2, municipioProvincia.lenght);
    var fechaInicio = $(".datoEditar"+ id +"[name='fechaInicio']").val();
    var fechaFin = $(".datoEditar"+ id +"[name='fechaFin']").val();
    var recIncluidos = $(".datoEditar"+ id +"[name='recIncluidos']").val();
    var recHechos = $(".datoEditar"+ id +"[name='recHechos']").val();
    /* MANDAR DATOS PARA EDITAR EL CLIENTE */
    $.ajax({
        url: clientesUpdateUrl,
        type: 'POST',
        data: {
            "_token": token,
            "id": id,
            "razon_social": razonSocial,
            "cif": cif,
            "direccion": direccion,
            "municipio": municipio,
            "provincia": provincia,
            "fecha_inicio": fechaInicio,
            "fecha_fin": fechaFin,
            "rec_incluidos": recIncluidos,
            "rec_hechos": recHechos,
        },
        success:function(result){
            if(result["status"]){
                actualizarFila(result["cliente"]);
            }
        }
    });
}

/* BOTONES DE PAGINAR */
function pasarPaginas(e, sumPages){
    var pagina = parseInt(e.id); //Recojo el número de la página sobre la que se ha pinchado
    var pagActual = $("#pagActual").val(); //Recojo la página actual
    if(pagActual != pagina){ // Compruebo que no se pinche sobre la página en la que se está
        if(pagActual < pagina){ // Pasar página hacia delante
            if(pagina >= 4 && pagina < sumPages){
                $("#pageStyle"+(pagina-2)).hide();
                $("#pageStyle"+(pagina+1)).show();
                $('#puntosSusp1').show();
            }
        }else{ // Pasar página hacia atrás
            if(pagina < 4){ // Cuando la página pinchada es menor de 4
                console.log(2);
                $(".enlacePagina").hide();
                $("#pageStyle1, #pageStyle2, #pageStyle3, #pageStyle4, #pageStyle"+sumPages).show();
                $('#puntosSusp1').hide();
            }else if(pagina > 4){ // Cuando la página pinchada es mayor de 4
                console.log(3);
                $("#pageStyle"+(pagina+1)).hide();
                $("#pageStyle"+(pagina-1)).show();
            }else{ // Cuando la página pinchada es concretamente 4
                $(".enlacePagina").hide();
                $("#pageStyle1, #puntosSusp1, #pageStyle3, #pageStyle4, #pageStyle5, #pageStyle"+sumPages).show();
            }
        }
    }
    $("#pagActual").val(pagina); //Actualizar valor de página actual
}